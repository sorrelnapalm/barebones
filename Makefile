cc=i686-elf-gcc
as=i686-elf-as
cflags=-std=gnu99 -ffreestanding -O2 -Wall -Wextra -Werror

.SUFFIXES: .s

.s.o:
	$(as) $< -o $@

asm=boot.s
src=kernel.c terminal.c util.c vga.c
headers=terminal.h util.h vga.h
obj = $(addsuffix .o, $(basename $(asm))) $(addsuffix .o, $(basename $(src)))

isopath=isodir
bootpath=$(isopath)/boot
grubpath=$(bootpath)/grub
qemu=qemu-system-i386

barebones.iso: barebones.bin
	mkdir -p $(grubpath)
	cp $< $(bootpath)
	cp grub.cfg $(grubpath)
	grub-mkrescue -o $@ $(isopath)

barebones.bin: $(obj)
	$(cc) $(cflags) -T linker.ld -o $@ -ffreestanding -O2 -nostdlib $(obj) -lgcc

%.o: %.c $(headers)
	$(cc) $(cflags) -c $< -o $@

run: barebones.iso
	$(qemu) -cdrom $<

runbin: barebones.bin
	$(qemu) -kernel $<

clean:
	rm -f $(obj)
	rm -rf $(isopath)

