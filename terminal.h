#ifndef _TERMINAL_H_
#define _TERMINAL_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

void terminal_init();
void terminal_setcolor(uint8_t color);
void terminal_putchar(char c);
void terminal_write(const char *data, size_t size);
void terminal_writestring(const char *data);


#endif // _TERMINAL_H_
