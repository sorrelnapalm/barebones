#ifndef _UTIL_H_

#include "util.h"

size_t
strlen(const char *str)
{
  size_t len = 0;
  while (str[len]) {
    len++;
  }

  return len;
}

#endif // _UTIL_H_
 
