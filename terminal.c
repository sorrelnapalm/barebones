#include "terminal.h"
#include "util.h"
#include "vga.h"

size_t terminal_row;
size_t terminal_column;
uint8_t terminal_color;
uint16_t *terminal_buffer;

extern const size_t VGA_HEIGHT;
extern const size_t VGA_WIDTH;

void
terminal_init()
{
  terminal_row = 0;
  terminal_column = 0;
  terminal_color = vga_entry_color(VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLACK);
  terminal_buffer = (uint16_t *) 0xb8000;
  for (size_t y = 0; y < VGA_HEIGHT; y++) {
    for (size_t x = 0; x < VGA_WIDTH; x++) {
      const size_t i = y * VGA_WIDTH + x;
      terminal_buffer[i] = vga_entry(' ', terminal_color);
    }
  }
}

void
terminal_setcolor(uint8_t color)
{
  terminal_color = color;
}

void
terminal_putentryat(char c, uint8_t color, size_t x, size_t y)
{
  const size_t i = y * VGA_WIDTH + x;
  terminal_buffer[i] = vga_entry(c, color);
}

void
terminal_putchar(char c)
{
  // handle special characters
  if (c == '\n') {
    terminal_column = VGA_WIDTH;
  } else {
    terminal_putentryat(c, terminal_color, terminal_column, terminal_row);
    terminal_column++;
  }

  if (terminal_column == VGA_WIDTH) {
    terminal_column = 0;
    terminal_row++;

    if (terminal_row == VGA_HEIGHT) {
      terminal_row = VGA_HEIGHT - 1;

      // scroll terminal

      // copy each row to the previous row
      for (size_t i = 1; i < VGA_HEIGHT; i++) {
        for (size_t j = 0; j < VGA_WIDTH; j++) {
          size_t index = i * VGA_WIDTH + j;
          terminal_buffer[index - VGA_WIDTH] = terminal_buffer[index];
        }
      }

      // clear last row
      for (size_t i = 0; i < VGA_WIDTH; i++) {
        terminal_putentryat(' ', terminal_color, i, VGA_HEIGHT - 1);
      }

    }
  }
}

void
terminal_write(const char *data, size_t size)
{
  for (size_t i = 0; i < size; i++) {
    terminal_putchar(data[i]);
  }
}

void
terminal_writestring(const char *data)
{
  terminal_write(data, strlen(data));
}

