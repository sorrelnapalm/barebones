#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "terminal.h"
#include "util.h"

#if defined(__linux__)
#error "You are not using a cross-compiler!"
#endif

#if !defined(__i386__)
#error "This tutorial needs to be compiled with a ix86-elf compiler!"
#endif


void kernel_main()
{
  terminal_init();
  terminal_writestring("Hello 0\n");
  terminal_writestring("Hello 1\n");
  terminal_writestring("Hello 2\n");
  terminal_writestring("Hello 3\n");
  terminal_writestring("Hello 4\n");
  terminal_writestring("Hello 5\n");
  terminal_writestring("Hello 6\n");
  terminal_writestring("Hello 7\n");
  terminal_writestring("Hello 8\n");
  terminal_writestring("Hello 9\n");
  terminal_writestring("Hello 10\n");
  terminal_writestring("Hello 11\n");
  terminal_writestring("Hello 12\n");
  terminal_writestring("Hello 13\n");
  terminal_writestring("Hello 14\n");
  terminal_writestring("Hello 15\n");
  terminal_writestring("Hello 16\n");
  terminal_writestring("Hello 17\n");
  terminal_writestring("Hello 18\n");
  terminal_writestring("Hello 19\n");
  terminal_writestring("Hello 20\n");
  terminal_writestring("Hello 21\n");
  terminal_writestring("Hello 22\n");
  terminal_writestring("Hello 23\n");
  terminal_writestring("Hello 24\n");
  terminal_writestring("Hello 25\n");
  terminal_writestring("Hello 26\n");
  terminal_writestring("Hello 27\n");
  terminal_writestring("Hello 28\n");
}

